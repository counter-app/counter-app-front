const axios = require('axios')

export default {
  counter: {
    get() {
      return axios.get('http://localhost:3000/api/counter')
      .then(function (response) {
        if (response.data) {
          return { error: 0, data: response.data, }
        } else {
          return { error: true, }
        }
      })
      .catch(function (error) {
        return { error: 1, message: error}
      })      
    },
    inc() {
      return axios.post('http://localhost:3000/api/counter/inc')
      .then(function (response) {
        if (response.data) {
          return { error: 0, data: response.data, }
        } else {
          return { error: true, }
        }
      })
      .catch(function (error) {
        return { error: 1, message: error}
      })      
    },
    dec() {
      return axios.post('http://localhost:3000/api/counter/dec')
      .then(function (response) {
        if (response.data) {
          return { error: 0, data: response.data, }
        } else {
          return { error: true, }
        }
      })
      .catch(function (error) {
        return { error: 1, message: error}
      })      
    },
  },
}

