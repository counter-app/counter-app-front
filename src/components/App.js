import React, { Component } from 'react';
import api from './../api/index'

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      points: 0,
    }

    this.onIncClick = this.onIncClick.bind(this)
    this.onDecClick = this.onDecClick.bind(this)
  }

  componentWillMount() {
    this.setCounter()
  }

  setCounter() {
    api.counter.get().then((response) => {
      if (response.error === 0 && response.data) {
        this.setState({
          points: response.data.points,
        })
      } else {
        console.error(response.message)
      }
    })
  }

  onIncClick() {
    api.counter.inc().then((response) => {
      if (response.error === 0 && response.data) {
        this.setState({
          points: response.data.counter.points,
        })
      } else {
        console.error(response.message)
      }
    })
  }

  onDecClick() {
    api.counter.dec().then((response) => {
      if (response.error === 0 && response.data) {
        this.setState({
          points: response.data.counter.points,
        })
      } else {
        console.error(response.message)
      }
    })
  }

  render() {
    return (
      <div className="App">
        <h2>Points: <span className="points">{this.state.points}</span></h2>
        <hr />
        <button onClick={this.onIncClick}>Increase</button>
        <hr />
        <button onClick={this.onDecClick}>Decrease</button>
      </div>
    );
  }
}

export default App;
